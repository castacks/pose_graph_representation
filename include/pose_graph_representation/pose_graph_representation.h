/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2017 Sankalp Arora
 * polar_2d_representation.h
 *
 *  Created on: Sep 3, 2017
 *      Author: Sankalp Arora
 */

#ifndef REPRESENTATION_INTERFACE_INCLUDE_POSE_GRAPH_REPRESENTATION_H
#define REPRESENTATION_INTERFACE_INCLUDE_POSE_GRAPH_REPRESENTATION_H

#include <representation_interface/representation_interface.h>
#include <pose_graph_representation/rectangle_object.hpp>
#include "polar_2d_map_representation/polar_2d_map_representation.h"
#include "visualization_msgs/MarkerArray.h"
#include "ros/ros.h"
namespace ca {
namespace representation_interface{
typedef std::pair<Eigen::Vector3d,double> ObservationPoint;
class PoseGraphRepresentation: public RepresentationInterface<double,3>{
// Assuming an NED frame
    double ConstraintAngle(double x){
        x = fmod(x + M_PI,2*M_PI);
        if (x < 0)
            x += 2*M_PI;
        return x - M_PI;
    }

    double max_radius_;
    double height_;
    double fov_;
    double range_;
    std::string frame_;
    std::vector<Eigen::Vector3d> cylinder_centers_;
    std::vector<double> cylinder_heights_;
    public:
    Polar_2D_Representation polar_grid_;
    bool polar_grid_initialized_;

    std::vector<ObservationPoint> sensing_points_;
    std::vector<RectangleObject> rect_objects_;
    //std::vector<size_t> ignore_list_;
    std::vector<bool> count_ignore_list_; // Its true if you want to ignore

    void SetIgnoreFrameList(std::vector<size_t>& ignore_list){
        count_ignore_list_.resize(sensing_points_.size(),false);
        for(auto i:ignore_list){
            count_ignore_list_[i] = true;
        }
    }

    void  InitializeIgnoreFrameList(){
        count_ignore_list_.resize(sensing_points_.size(),false);
    }

    void ClearIgnoreFrameList(){
        count_ignore_list_.resize(sensing_points_.size(),false);
    }

    double Pos2Angle(const Eigen::Vector3d &v, Eigen::Vector3d &center){
        Eigen::Vector3d angle_vec = v - center;
        double angle = std::atan2(angle_vec.y(),angle_vec.x());
        return angle;
    }


  PoseGraphRepresentation(){
      polar_grid_initialized_ = false;
  }

  void AddCylinders(Eigen::Vector3d cylinder_center){
      cylinder_centers_.push_back(cylinder_center);
      cylinder_heights_.push_back(height_);
  }

  void AddRectangle(RectangleObject &rect){
      rect_objects_.push_back(rect);
  }

  void AddRectangles(std::vector<RectangleObject> &rect_vec){
      for(auto i:rect_vec)
          rect_objects_.push_back(i);
  }


  void AddCylinders(Eigen::Vector3d cylinder_center, double cylinder_height){
      cylinder_centers_.push_back(cylinder_center);
      cylinder_heights_.push_back(cylinder_height);
  }


  void AddSensingPoint(Eigen::Vector3d centers, double heading){
    sensing_points_.push_back(std::make_pair(centers,heading));
  }



  bool IsInSensingPoint(const Eigen::Vector3d& pos, ObservationPoint& ob);

  bool IsPolarGridInitialized(){
      return polar_grid_initialized_;
  }

  bool IsInCylinder(Eigen::Vector3d pos);

  bool IsInBox(Eigen::Vector3d pos){
      //ROS_INFO_STREAM("Box check");
      for(auto i:rect_objects_){
          if(i.InCollision(pos))
              return true;
      }
      return false;
  }

  bool FindIntersection(Eigen::Vector3d start_p, Eigen::Vector3d end_p, double radius, Eigen::Vector3d &intersection_point, bool first);

  bool FindIntersectionOneCylinder(Eigen::Vector3d start_p, Eigen::Vector3d end_p, double radius,
                                   Eigen::Vector3d center, double height,Eigen::Vector3d &intersection_point);


  void SetParams(double max_radius, double height, double fov, double range, std::string frame);

  void SetPolarGridParams(double angle_resolution, double max_radius, double arc_size, double arc_center, Eigen::Vector3d &center, double height,
                     std::string frame){
      polar_grid_.SetParams(angle_resolution, max_radius, arc_size, arc_center, center, height, frame);
      polar_grid_initialized_ = true;
      AddCylinders(center,height);
  }
  double GetRadius(){return max_radius_;}

  ~PoseGraphRepresentation(){}

  /**
   * \brief initializes the interface class returns false if can't initialize
   * @param
   * @return true if initialization was successful, false otherwise
   */
  virtual bool Initialize(){return true;}

  /** \brief Get value at query location.
   *
   * @param query Eigen column vector representing a general query
   * @return value at location
   */
  virtual bool GetValue(const Eigen::Vector3d  &query, double &value);
  /** \brief Get value at query location.
   *
   * @param query Eigen column vector representing a general query
   * @return value at location
   */
  virtual bool GetValue(const Eigen::Vector3f  &query, double &value){
      ROS_ERROR_STREAM("PoseGraphRepresentation, There is no int getvalue function for this!");
      std::exit(-1);
     return false;

  }
  /** \brief Get value at query index.
   *
   * @param query Eigen column vector representing a general query
   * @return value at location
   */
  virtual bool GetValue(const Eigen::Vector3i  &query, double &value){
      if(polar_grid_initialized_){
         return polar_grid_.GetValue(query,value);
      }
      return false;
  }
  /** \brief Get value at query locations.
   *
   * @param query a vector of Eigen columns vector representing a general query index.
   * @return a vector of values at query locations.
   */
  virtual std::vector<std::pair<double, bool> > GetValue(const std::vector<Eigen::Vector3d > &query);
    /** \brief Get value at query indices.
   *
   * @param query a vector of Eigen columns vector representing general query indices.
   * @return a vector of values at query locations.
   */
  virtual std::vector<std::pair<double, bool> > GetValue(const std::vector<Eigen::Vector3i > &query){
      ROS_ERROR_STREAM("PoseGraphRepresentation, There is no int getvalue function for this!");
      std::exit(-1);
     //return false;
  }
  /**
   * \brief Is the query location valid.
   * @param query
   * @return boolean whether the query is valid or not
   */
  virtual bool IsValid(const Eigen::Vector3d  &query){return true; }
  /**
    * \brief Are the query indices valid.
    * @param query
    * @return boolean whether the query is valid or not
    */
  virtual bool IsValid(const Eigen::Vector3i  &query){ return true;}
  /**
   * \brief Returns the minimum valid query that can be made to the representation
   * @param
   * @return minimum valid query location.
   */
  virtual Eigen::Vector3d GetMinQuery(){ return Eigen::Vector3d(0,0,0);}
  /**
   * \brief Returns the maximum valid query
   * @param
   * @return maximum valid query location.
   */
  virtual Eigen::Vector3d GetMaxQuery(){ return Eigen::Vector3d(0,0,0);}
  /**
   * \brief Returns the resolution at which the grid locally operates at that location
   * @param query
   * @return resolution
   */
  virtual Eigen::Vector3d GetResolution(const Eigen::Vector3d &query){ return Eigen::Vector3d(0,0,0);}
  /**
   * \brief Returns the resolution at which the grid locally operates at that index
   * @param query
   * @return resolution
   */
  virtual Eigen::Vector3d GetResolution(const Eigen::Vector3i &query){return Eigen::Vector3d(0,0,0);}
  /**
   * \brief returns the frame in which the representation exists,
   * @param
   * @return the frames in which the representation exists
   */
  virtual std::string get_frame(){return frame_;}

  visualization_msgs::MarkerArray GetCylinder(int id);
  visualization_msgs::Marker GetSectors(int id);
  visualization_msgs::Marker GetRectangles(int id);
  void set_frame(std::string temp_frame){frame_ = temp_frame;}
};

}  // namepsace ca
}  // namespace representation_interface


#endif
