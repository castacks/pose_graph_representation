#ifndef RECTANGLE_OBJECT_POSE_GRAPH_REPRESENTATION
#define RECTANGLE_OBJECT_POSE_GRAPH_REPRESENTATION

#include <cmath>
#include <vector>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <tf/transform_listener.h>


namespace ca
{

template<typename Scalar>
class Ray3 {
 public:
  typedef Eigen::Matrix<Scalar, 3, 1> Vec3;

 public:

  Ray3(const Vec3& origin,
       const Vec3& direction) :
         origin(origin),
         direction(direction),
         tmin(Scalar(0)),
         tmax(std::numeric_limits<Scalar>::max())
  {
    invdir = invdir.cwiseInverse();
    sign[0] = (invdir.x() < 0);
    sign[1] = (invdir.y() < 0);
    sign[2] = (invdir.z() < 0);
  }

  Ray3(const Vec3& origin,
       const Vec3& direction,
       Scalar tmin,
       Scalar tmax) :
         origin(origin),
         direction(direction),
         tmin(tmin),
         tmax(tmax)
  {
    invdir = invdir.cwiseInverse();
    sign[0] = (invdir.x() < 0);
    sign[1] = (invdir.y() < 0);
    sign[2] = (invdir.z() < 0);
  }

  Ray3(const Ray3<Scalar>& other) {
    if (this == &other) { return; }

    origin = other.origin;
    direction = other.direction;
    tmin = other.tmin;
    tmax = other.tmax;
    invdir = other.invdir;
    sign[0] = other.sign[0];
    sign[1] = other.sign[1];
    sign[2] = other.sign[2];
  }

  Ray3<Scalar>& operator=(const Ray3<Scalar>& other) {
    // TODO wait
    if (this == &other) { return *this; }

    origin = other.origin;
    direction = other.direction;
    tmin = other.tmin;
    tmax = other.tmax;
    invdir = other.invdir;
    sign[0] = other.sign[0];
    sign[1] = other.sign[1];
    sign[2] = other.sign[2];

    return *this;
  }

 public:
  Vec3 PointAt(Scalar t) const {
    return origin + t*direction;
  }

 public:
  Vec3 origin, direction;
  Scalar tmin, tmax; /// ray min and max distances
  Vec3 invdir; // for convenience in AABB intersection
  int sign[3];

};

typedef Ray3<float> Ray3f;
typedef Ray3<double> Ray3d;

} /* ca */



namespace ca
{

struct IntersectionInfo {
  IntersectionInfo() :
      point(0,0,0),
      range(0),
      intensity(0),
      label(0)
  { }
  Eigen::Vector3d point; ///< location of intersection in /world
  double range; ///< range from scanner
  float intensity; ///< pulse intensity
  uint8_t label; ///< object label of hit
  double time_s; ///< time stamp in seconds
};


class RectangleObject {
 public:
  typedef boost::shared_ptr<RectangleObject> Ptr;

 private:

  // Note one set of tf parameters is in world frame.
  // The other is in object-centric frame.
  // TODO clean up this tf mess.
  std::string name_;
  std::string frame_;
  Eigen::Vector3d center_;
  Eigen::Vector3d points_[4];
  Eigen::Quaterniond orientation_;

  Eigen::Quaterniond rotation_;
  Eigen::Vector3d translation_;
  Eigen::Vector3d normal_;

  double length_;
  double breadth_;
  double lengthBy2_;
  double breadthBy2_;
  double collision_expansion_;

  std_msgs::ColorRGBA marker_rgba_;
  uint8_t label_;

 private:
  void init();

 public:

  RectangleObject(const std::string& name,
                  ros::NodeHandle &n);

  RectangleObject(const std::string& name,
                  double length,
                  double breadth,
                  Eigen::Quaterniond orientation,
                  Eigen::Vector3d position,
                  double collision_expansion,
                  std::string frame);

  RectangleObject(){ }

  ~RectangleObject() { }

 public:

  void set_marker_rgba(const std_msgs::ColorRGBA& rgba) {
    marker_rgba_ = rgba;
  }

  void set_marker_rgba(const Eigen::Vector4f& marker_rgba) {
    marker_rgba_.r = marker_rgba[0];
    marker_rgba_.g = marker_rgba[1];
    marker_rgba_.b = marker_rgba[2];
    marker_rgba_.a = marker_rgba[3];
  }

  void set_marker_alpha(float alpha) {
    marker_rgba_.a = alpha;
  }

  void set_marker_rgb(const Eigen::Vector3f& rgb) {
    marker_rgba_.r = rgb[0];
    marker_rgba_.g = rgb[1];
    marker_rgba_.b = rgb[2];
  }

  void set_size(double length, double breadth);

  void set_label(uint8_t label) {
    label_ = label;
  }

  void MoveRectangle(geometry_msgs::Pose p);

  void GetMarker(visualization_msgs::Marker& marker);

  bool Intersect(const ca::Ray3d& ray, ca::IntersectionInfo& iinfo, bool collision_call = false);

  bool InCollision(Eigen::Vector3d pos);

};

} /* CA */

#endif /* end of include guard: RECTANGLE_RAYCASTER_H_GDL2NFRB */
