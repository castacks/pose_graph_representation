#include "pose_graph_representation/pose_graph_representation.h"

namespace ca_ri = ca::representation_interface;
visualization_msgs::MarkerArray ca_ri::PoseGraphRepresentation::GetCylinder(int id){
    visualization_msgs::Marker m;
    m.header.frame_id = frame_;
    m.header.stamp = ros::Time();
    m.ns = "cylinder";
    m.frame_locked = true;
    m.id = id;
    m.type = visualization_msgs::Marker::CYLINDER;
    m.action = visualization_msgs::Marker::ADD;

    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 2*max_radius_;
    m.scale.y = 2*max_radius_;
    m.scale.z = height_;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 0.0;
    m.color.b = 0.0;

    visualization_msgs::MarkerArray ma;
    for(size_t i=0; i<cylinder_centers_.size(); i++){
        m.id = id+i;
        m.pose.position.x = cylinder_centers_[i].x();
        m.pose.position.y = cylinder_centers_[i].y();
        m.pose.position.z = cylinder_centers_[i].z()- 0.5*cylinder_heights_[i];
        m.scale.z  = cylinder_heights_[i];
        ma.markers.push_back(m);
    }

    visualization_msgs::Marker m_sphere;
    m_sphere.header.frame_id = frame_;
    m_sphere.header.stamp = ros::Time();
    m_sphere.ns = "cylinder_viz";
    m_sphere.frame_locked = true;
    m_sphere.id = id;
    m_sphere.type = visualization_msgs::Marker::SPHERE_LIST;
    m_sphere.action = visualization_msgs::Marker::ADD;

    m_sphere.pose.orientation.x = 0.0;
    m_sphere.pose.orientation.y = 0.0;
    m_sphere.pose.orientation.z = 0.0;
    m_sphere.pose.orientation.w = 1.0;
    m_sphere.scale.x = 2*max_radius_;
    m_sphere.scale.y = 2*max_radius_;
    m_sphere.scale.z = std::min(0.1*height_,5.0);
    //ROS_INFO_STREAM("M sphere scale z::"<<m_sphere.scale.z);
    m_sphere.color.a = 1.0; // Don't forget to set the alpha!
    m_sphere.color.r = 1.0;
    m_sphere.color.g = 0.0;
    m_sphere.color.b = 0.0;
    geometry_msgs::Point p;
    for(size_t i=0; i<cylinder_centers_.size(); i++){
        p.x = cylinder_centers_[i].x();
        p.y = cylinder_centers_[i].y();
        p.z = cylinder_centers_[i].z()- 0.5*height_;
        m_sphere.points.push_back(p);
    }
    ma.markers.push_back(m_sphere);
    //m.points.push_back(p);
    return ma;
}
visualization_msgs::Marker ca_ri::PoseGraphRepresentation::GetSectors(int id){
    visualization_msgs::Marker m;
    m.header.frame_id = frame_;
    m.header.stamp = ros::Time();
    m.ns = "sectors";
    m.frame_locked = true;
    m.id = id;
    m.type = visualization_msgs::Marker::TRIANGLE_LIST;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0;
    m.pose.position.y = 0;
    m.pose.position.z = 0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 1;
    m.scale.y = 1;
    m.scale.z = 1;
    m.color.a = 0.1; // Don't forget to set the alpha!
    m.color.r = 0.0;
    m.color.g = 1.0;
    m.color.b = 0.0;
    double angle = 0;
    std_msgs::ColorRGBA c; c.r = 0; c.g = 1.0; c.b = 0; c.a = 0.5;
    Eigen::Vector3d v1, v2, v3;
    Eigen::Vector3d intersection_point;
    //double r0,r1;
    for(size_t i=0; i<sensing_points_.size(); i++){
        //ROS_ERROR_STREAM("Center::"<<center_);
        if(!(count_ignore_list_.size()>i &&count_ignore_list_[i])){
            angle = sensing_points_[i].second;
            //ROS_ERROR_STREAM("Angle::"<<i<<"::"<<angle<<"::"<<fov_<<"::"<<range_);
            double start_angle = angle - 0.5*fov_;
            double end_angle = angle + 0.5*fov_;
            double angle_res = fov_/3;

            v1 = sensing_points_[i].first;
            for(angle=start_angle; angle<end_angle; angle = angle+angle_res){
                v2 = sensing_points_[i].first + range_*Eigen::Vector3d(std::cos(angle),std::sin(angle),0);
                if(FindIntersection(v1, v2, range_, intersection_point, false)){
                    v2 = intersection_point;
                }

                v3 = sensing_points_[i].first + range_*Eigen::Vector3d(std::cos(angle+angle_res),std::sin(angle+angle_res),0);
                if(FindIntersection(v1, v3, range_, intersection_point, false)){
                    v3 = intersection_point;
                }


                geometry_msgs::Point p; p.x = v1.x(); p.y = v1.y(); p.z = v1.z();
                m.points.push_back(p);
                p.x = v2.x(); p.y = v2.y(); p.z = v2.z();
                m.points.push_back(p);
                p.x = v3.x(); p.y = v3.y(); p.z = v3.z();
                m.points.push_back(p);
            }
        }
    }
    //m.points.push_back(p);
    return m;
}

visualization_msgs::Marker ca_ri::PoseGraphRepresentation::GetRectangles(int id){
    visualization_msgs::Marker m;
    visualization_msgs::Marker m1;
    for(size_t i=0; i<rect_objects_.size();i++){
        ca::RectangleObject& rec = rect_objects_[i];
        if(i==0)
            rec.GetMarker(m);
        else{
            rec.GetMarker(m1);
            for(auto i:m1.points)
                m.points.push_back(i);
        }
    }
    m.id = id;
    return m;
}

bool ca_ri::PoseGraphRepresentation::IsInSensingPoint(const Eigen::Vector3d &pos, ObservationPoint& ob){
    //ROS_INFO_STREAM("IsInSensingPoint");
    Eigen::Vector3d p = pos-ob.first;
    if(p.norm() < 1e-3){
        //ROS_INFO_STREAM("Too close");
        return true;
    }

    double angle = atan2(p.y(),p.x());
    if(p.norm() > range_){
        //ROS_INFO_STREAM("Too far");
        return false;
    }
    angle = ConstraintAngle(angle - ob.second);
    if(fabs(angle) > 0.5*fov_){
        //ROS_INFO_STREAM("Too wide");
        return false;
    }

    Eigen::Vector3d intersection_pt;
    if(FindIntersection(ob.first, pos, max_radius_, intersection_pt,true)){
        //ROS_INFO_STREAM("In the shadow");
        return false;
    }

    return true;
}

bool ca_ri::PoseGraphRepresentation::IsInCylinder(Eigen::Vector3d pos){
    for(size_t i=0; i< cylinder_centers_.size(); i++){
        Eigen::Vector3d center = cylinder_centers_[i];
        if(((center.z()-pos.z()) <= height_) && (center.z()>=pos.z())){
            pos.z() = center.z();
            if((center-pos).norm() <= max_radius_ ){
                return true;
            }
        }
    }
    return false;
}

bool ca_ri::PoseGraphRepresentation::FindIntersection(Eigen::Vector3d start_p, Eigen::Vector3d end_p,
                                                      double radius, Eigen::Vector3d &intersection_point, bool first){
    Eigen::Vector3d center;
    double height;
    Eigen::Vector3d closest_intersection_point;
    double closest_distance=1000000;
    double min_closest_distance = 0.1;
    double distance;

    Eigen::Vector3d direction = end_p - start_p;
    double ray_range = direction.norm(); direction.normalize();
    ca::Ray3d ray(start_p, direction);
    ca::IntersectionInfo iinfo;
    for(size_t i=0; i<rect_objects_.size(); i++){
        RectangleObject& ro = rect_objects_[i];
        if(ro.Intersect(ray,iinfo)){
            if(iinfo.range < min_closest_distance){
                intersection_point = start_p;
                return true;
            }
            if(iinfo.range < closest_distance && iinfo.range < ray_range){
                closest_distance = iinfo.range;
                closest_intersection_point = iinfo.point;
            }
        }
    }

    for(size_t i=0; i< cylinder_centers_.size(); i++){
        center = cylinder_centers_[i]; height = cylinder_heights_[i];
        //ROS_INFO_STREAM("In center list["<<i<<"] Center::"<<center.x()<<"::"<<center.y()<<"::"<<center.z());
        //ROS_INFO_STREAM("Start P::"<<start_p.x()<<"::"<<start_p.y()<<"::"<<start_p.z());
        //ROS_INFO_STREAM("End P::"<<end_p.x()<<"::"<<end_p.y()<<"::"<<end_p.z());
        if(FindIntersectionOneCylinder(start_p, end_p, radius, center, height,intersection_point)){
            distance = (intersection_point-start_p).norm();
            if(first) return true;
            if(distance < closest_distance){
                closest_distance = distance;
                closest_intersection_point = intersection_point;
            }
        }
    }

    intersection_point = closest_intersection_point;
    if(closest_distance < 100000)
        return true;

    return false;
}


bool ca_ri::PoseGraphRepresentation::FindIntersectionOneCylinder(Eigen::Vector3d start_p, Eigen::Vector3d end_p, double radius,
                                                                 Eigen::Vector3d center, double height, Eigen::Vector3d &intersection_point){

    // Making the cylinder center to be 0,0
    Eigen::Vector3d p1 = start_p - center;
    Eigen::Vector3d p2 = end_p - center;
    Eigen::Vector3d d = end_p - start_p;
    double D = p1.x()*p2.y() - p2.x()*p1.y();
    double dr = std::pow(d.x(),2) + std::pow(d.y(),2);// std::sqrt(dr);
    double discriminant = radius*radius*dr-D*D;
    Eigen::Vector3d p3,p4;
    if(discriminant<0){
        return false;
    }
    else if(discriminant == 0){
        p3.x() = D*d.y()/dr;
        p3.y() = -D*d.x()/dr;
        p4 = p3;
    }
    else{
        discriminant = std::sqrt(discriminant);
        if(d.y() < 0){
            p3.x() = (D*d.y() + (-1)*d.x()*discriminant)/dr;
            p3.y() = (-D*d.x() + std::abs(d.y())*discriminant)/dr;

            p4.x() = (D*d.y() - (-1)*d.x()*discriminant)/dr;
            p4.y() = (-D*d.x() - std::abs(d.y())*discriminant)/dr;

        }else{
            p3.x() = (D*d.y() + d.x()*discriminant)/dr;
            p3.y() = (-D*d.x() + std::abs(d.y())*discriminant)/dr;

            p4.x() = (D*d.y() - d.x()*discriminant)/dr;
            p4.y() = (-D*d.x() - std::abs(d.y())*discriminant)/dr;
        }
    }

    p3 = p3 + center;
    p4 = p4 + center;

    if(std::abs(d.x())>0){
        double lambda = (p3.x() - start_p.x())/d.x();
        p3.z() = start_p.z() + lambda*d.z();

        lambda = (p4.x() - start_p.x())/d.x();
        p4.z() = start_p.z() + lambda*d.z();

    }
    else if(std::abs(d.y())>0){
        double lambda = (p3.y() - start_p.y())/d.y();
        p3.z() = start_p.z() + lambda*d.z();

        lambda = (p4.y() - start_p.y())/d.y();
        p4.z() = start_p.z() + lambda*d.z();

    }
    else{
        return false;
    }

    //ROS_ERROR_STREAM("---------------Reaching this intersection point::---------------");
    Eigen::Vector3d closest_intersection;
    if((p3 - start_p).norm() < (p4 - start_p).norm())
        closest_intersection  = p3;
    else
        closest_intersection = p4;

    //ROS_ERROR_STREAM("Height_::"<<height<<"Intersection z::"<<closest_intersection.z()<<" Center z::"<<center.z());
    if(closest_intersection.z() < center.z() &&
            closest_intersection.z() > (center.z()-height)){
        //ROS_ERROR_STREAM("----------HEIGHT WORKS-----------");
        intersection_point = closest_intersection;
        Eigen::Vector3d v1 = intersection_point-start_p;
        Eigen::Vector3d v2 = end_p-start_p;
        double dot_p = v2.dot(v1);
        double line_length = v2.norm();
        if((dot_p > 0) && (dot_p <= (line_length*line_length)))
            return true;
    }

    return false;
}

void ca_ri::PoseGraphRepresentation::SetParams(double max_radius,
             double height, double fov, double range, std::string frame){
    frame_ = frame;
    cylinder_centers_.clear();
    sensing_points_.clear();
    max_radius_ = max_radius;
    fov_ = fov;
    height_ = height;
    range_ = range;
}

bool ca_ri::PoseGraphRepresentation::GetValue(const Eigen::Vector3d  &query, double &value){
    if(IsInCylinder(query) || IsInBox(query)){
        value = 1;
        return true;
    }

    //bool use_ignore_list = (sensing_points_.size()==count_ignore_list_.size());
    for(size_t i=0; i<sensing_points_.size(); i++){
        //ObservationPoint p = sensing_points_[i];
        if(!(count_ignore_list_.size()>i &&count_ignore_list_[i]) && IsInSensingPoint(query,sensing_points_[i])){
            //ObservationPoint& p = sensing_points_[i];
            //ROS_INFO_STREAM("["<<i<<"] Queried SP::"<<p.first.x()<<"::"<<p.first.y()<<"::"<<p.first.z());
            value = -1;
            return true;
        }
    }
    value = 0;
    return false;
}

std::vector<std::pair<double, bool> > ca_ri::PoseGraphRepresentation::GetValue(const std::vector<Eigen::Vector3d > &query){
      std::pair<double, bool> p(0,true);
      std::vector<std::pair<double, bool> > vector_p;
      for(auto i=0; i< query.size(); i++){
        p.second = GetValue(query[i],p.first);
        vector_p.push_back(p);
      }
      return vector_p;
}
