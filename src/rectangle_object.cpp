#include <pose_graph_representation/rectangle_object.hpp>

#include <geom_cast/geom_cast.hpp>

namespace ca
{

RectangleObject::RectangleObject(const std::string& name,
                                 ros::NodeHandle& n)
{

  bool parameters_ok = true;
  name_ = name;

  parameters_ok = parameters_ok && n.getParam((name_+"/length"), length_);
  lengthBy2_ = length_/2;
  parameters_ok = parameters_ok && n.getParam((name_+"/breadth"), breadth_);
  breadthBy2_ = breadth_/2;

  parameters_ok = parameters_ok && n.getParam(name_+"/center_x", center_.x());
  parameters_ok = parameters_ok && n.getParam(name_+"/center_y", center_.y());
  parameters_ok = parameters_ok && n.getParam(name_+"/center_z", center_.z());

  parameters_ok = parameters_ok && n.getParam(name_+"/quaternion_x", orientation_.x());
  parameters_ok = parameters_ok && n.getParam(name_+"/quaternion_y", orientation_.y());
  parameters_ok = parameters_ok && n.getParam(name_+"/quaternion_z", orientation_.z());
  parameters_ok = parameters_ok && n.getParam(name_+"/quaternion_w", orientation_.w());
  parameters_ok = parameters_ok && n.getParam(name_+"/collision_expansion", collision_expansion_);

  if (!parameters_ok) {
    ROS_ERROR("Error while reading param file for rectangle known as %s", name_.c_str());
    exit(0);
  }

  label_ = 0;

  this->init();
}

RectangleObject::RectangleObject(const std::string& name,
                                 double length,
                                 double breadth,
                                 Eigen::Quaterniond orientation,
                                 Eigen::Vector3d position,
                                 double collision_expansion,
                                 std::string frame)
{
  name_ = name;

  length_ = length;
  breadth_ = breadth;
  lengthBy2_ = length_/2;
  breadthBy2_ = breadth_/2;

  center_ = position;
  orientation_ = orientation;

  label_ = 0;

  frame_ = frame;

  collision_expansion_ = collision_expansion;
  this->init();
}

void RectangleObject::init() {

  // TODO this a stupid inverse
  rotation_ = orientation_.inverse();
  translation_ = -center_;

  normal_ = Eigen::Vector3d(0, 0, 1);

  points_[0] = Eigen::Vector3d( lengthBy2_,  breadthBy2_, 0);
  points_[1] = Eigen::Vector3d( lengthBy2_, -breadthBy2_, 0);
  points_[2] = Eigen::Vector3d(-lengthBy2_, -breadthBy2_, 0);
  points_[3] = Eigen::Vector3d(-lengthBy2_,  breadthBy2_, 0);

  for(int i=0; i<4 ; i++) {
    points_[i] = orientation_*points_[i];
    points_[i] += center_;
  }

  // default color
  marker_rgba_.r = 68.0/255.0;
  marker_rgba_.g = 170.0/255.0;
  marker_rgba_.b = 221.0/255.0;
  marker_rgba_.a = 0.7;
}

// TODO should this call init()?
void RectangleObject::set_size(double length, double breadth) {
  length_ = length;
  breadth_ = breadth;
  lengthBy2_ = length/2;
  breadthBy2_ = breadth/2;
}

void RectangleObject::MoveRectangle(geometry_msgs::Pose p) {
  center_ = ca::point_cast< Eigen::Vector3d >(p.position);
  orientation_ = ca::rot_cast< Eigen::Quaterniond >(p.orientation);

  this->init();
}

void RectangleObject::GetMarker(visualization_msgs::Marker& marker) {

  //TODO what frame? world/raycasting frame for now
  //marker.header.frame_id = this->raycasting_frame_id();
  marker.header.stamp = ros::Time::now();
  marker.ns = name_; marker.header.frame_id = frame_;
  marker.id = 1;
  marker.type = visualization_msgs::Marker::TRIANGLE_LIST;
  marker.points.clear();
  marker.points.reserve(6);
  marker.points.push_back(ca::point_cast<geometry_msgs::Point>(points_[0]));
  marker.points.push_back(ca::point_cast<geometry_msgs::Point>(points_[1]));
  marker.points.push_back(ca::point_cast<geometry_msgs::Point>(points_[2]));
  marker.points.push_back(ca::point_cast<geometry_msgs::Point>(points_[0]));
  marker.points.push_back(ca::point_cast<geometry_msgs::Point>(points_[2]));
  marker.points.push_back(ca::point_cast<geometry_msgs::Point>(points_[3]));

  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.position = ca::point_cast<geometry_msgs::Point>(Eigen::Vector3d(0, 0, 0));
  marker.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(Eigen::Quaterniond(1, 0, 0, 0));

  marker.scale.x = 1;
  marker.scale.y = 1;
  marker.scale.z = 1;

  marker.color = marker_rgba_;

  marker.frame_locked = 1.0;
}

bool RectangleObject::Intersect(const ca::Ray3d& ray,
                                ca::IntersectionInfo& iinfo, bool collision_call) {

  //Eigen::Quaterniond q(ca::rot_cast<Eigen::Quaterniond>(tf_laser_to_raycasting_.getRotation()));
  //ROS_ASSERT( fabs(q.norm()-1.0) < 1e-10 );
  //Eigen::Vector3d vec_length = q*laser_vector;

  if(!collision_call && InCollision(ray.origin)){
      iinfo.range = 0.;
      iinfo.point = ray.origin;
      iinfo.label = label_;
      return true;
  }

  Eigen::Vector3d v = rotation_*ray.direction;
  Eigen::Vector3d a(translation_+ray.origin);
  a = rotation_*a;
  double t = -a.z()/v.z();
  a += t*v;

  if (a.x()>-lengthBy2_ && a.x()<lengthBy2_ &&
      a.y()>-breadthBy2_ && a.y()<breadthBy2_ &&
      a.z()>-1e-06 && a.z()<1e-06 &&
      t > 0 &&
      t < ray.tmax) {
    iinfo.range = t; // TODO is direction always unit?
    iinfo.point = ray.PointAt(t);
    iinfo.label = label_;
    // TODO intensity
    return true;
  }
  return false;
}

bool RectangleObject::InCollision(Eigen::Vector3d pos){
    //ROS_INFO_STREAM("InCollision");
    Eigen::Vector3d ray_dir = orientation_*Eigen::Vector3d(0,0,1);
    ca::Ray3d ray(pos,ray_dir);
    ca::IntersectionInfo iinfo;
    if(Intersect(ray,iinfo, true)){
        if(iinfo.range<= collision_expansion_)
            return true;
    }
    else{
        ray.direction = -ray.direction;
        if(Intersect(ray,iinfo,true)){
            if(iinfo.range<= collision_expansion_)
                return true;
        }
    }
    return false;
}
} // namespace
