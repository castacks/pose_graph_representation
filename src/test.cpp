#include "ros/ros.h"
#include "pose_graph_representation/pose_graph_representation.h"
#include "visualization_msgs/MarkerArray.h"

namespace ca_ri = ca::representation_interface;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "talker");
    ros::NodeHandle n;


    ros::Publisher marker_pub = n.advertise<visualization_msgs::MarkerArray>("pose_graph_representation", 1000);
    ros::Rate loop_rate(1);

    ca_ri::PoseGraphRepresentation  pose_graph_rep;
    //Eigen::Vector3d center(0,0,0);
    pose_graph_rep.SetParams(1,1.5,M_PI/3,25.0,"/world");
    int count = 0;
    visualization_msgs::MarkerArray ma;
    visualization_msgs::Marker m;

    //pose_graph_rep.AddCylinders(Eigen::Vector3d(0,0,0));
    //pose_graph_rep.AddCylinders(Eigen::Vector3d(10,0.,0.1));

    pose_graph_rep.AddSensingPoint(Eigen::Vector3d(-2,0,0.),0.);
    pose_graph_rep.AddSensingPoint(Eigen::Vector3d(5,0,0.),1.57);

    ca::RectangleObject ro("rect",20.,7.,Eigen::Quaterniond(1/sqrt(2),0,1/sqrt(2),0),Eigen::Vector3d(15.,0.,0.),0.2,"/world");
    pose_graph_rep.AddRectangle(ro);

    //ca_ri::Polar_2D_Representation ri;
    double angular_resolution = M_PI*(60/180);
    double max_radius = 2;
    double arc_size = 2*M_PI;
    double arc_origin = -M_PI;
    Eigen::Vector3d center(17.,0.,5.0);
    pose_graph_rep.SetPolarGridParams(angular_resolution,max_radius,arc_size,arc_origin,center,20.0,"/world");


    Eigen::Vector3d v(0.0,0,0); double value=0;
    pose_graph_rep.GetValue(v,value);
    ROS_INFO_STREAM("1::"<<value);

    Eigen::Vector3d v1(13.0,0,0);
    pose_graph_rep.GetValue(v1,value);
    ROS_INFO_STREAM("2::"<<value);

    Eigen::Vector3d v2(13.0,3.4,0);
    pose_graph_rep.GetValue(v2,value);
    ROS_INFO_STREAM("3::"<<value);

    Eigen::Vector3d v3(15.5,3.4,0);
    pose_graph_rep.GetValue(v3,value);
    ROS_INFO_STREAM("4::"<<value);

    Eigen::Vector3d v4(15.1,0.,0.);
    pose_graph_rep.GetValue(v4,value);
    ROS_INFO_STREAM("5::"<<value);

    Eigen::Vector3d pos(0.,0.,0.);

    if(pose_graph_rep.IsInCylinder(pos) || pose_graph_rep.IsInBox(pos)){
        ROS_INFO_STREAM("Its in stuff");
    }
    else{
        double heading = 0.0;
        Eigen::Quaterniond  q(0.0,0.0,0.0,0.0);
        q.w() = std::cos(0.5*heading); q.z() = std::sin(0.5*heading);
        Eigen::Vector3d vv(1.,0.,0.);
        Eigen::Vector3d end_pos,intersection_pt;
        double radius = 0.9*pose_graph_rep.polar_grid_.GetRadius();
        end_pos = pos + 100.*vv;

        if(pose_graph_rep.IsPolarGridInitialized() && pose_graph_rep.FindIntersection(pos,end_pos,radius,intersection_pt,false)){
            size_t id;
            if(pose_graph_rep.polar_grid_.GetIDs(intersection_pt,id)){
                ROS_INFO_STREAM("This was visible::"<<id);
            }
        }
    }




    while (ros::ok())
    {
        ma = pose_graph_rep.GetCylinder(0);
        m = pose_graph_rep.GetSectors(0);
        ma.markers.push_back(m);
        m = pose_graph_rep.GetRectangles(0);
        ma.markers.push_back(m);
        marker_pub.publish(ma);
        ros::spinOnce();
        loop_rate.sleep();
        ++count;
    }


    return 0;
}
